import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
// import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ChatComponent } from './chatwidget/chat.component';
import { createCustomElement} from '@angular/elements';

@NgModule({
  declarations: [
    // AppComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [ChatComponent]
})
export class AppModule { 
  constructor(
    private injector: Injector
  ){

  }

  ngDoBootstrap(){
    const chatcomponent = createCustomElement(ChatComponent, {injector: this.injector});
    customElements.define('paybotus-bot', chatcomponent);
  }

}
